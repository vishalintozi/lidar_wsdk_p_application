﻿【Demo功能】
1、Demo介绍了SDK初始化、登陆设备、登出设备、云台操作等功能。

【注意事项】
1、此Demo只支持云台设备
2、运行前请把压缩包内"General_NetSDK_Chn_LinuxXXX_IS_VXXX.R.XXX.tar.gz\库文件"里所有的so文件复制到"\02.PTZControl"目录中，不要有遗漏so文件，以防启动程序时提示找不到依赖的库文件。
3、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1、Demo SDK initialization,login device, logout device, PTZ control function.

【NOTE】
1、Only support PTZ Devices. 
2、Copy All .so files in the "General_NetSDK_Eng_LinuxXXX_IS_VXXX.R.XXX.tar.gz/bin" directory into the directory where the program is built,that in the "\02.PTZControl" path , before running. To avoid prompting to cannot find the dependent .so files when the program start.
3、If run the program with some problems,please go to http://www.dahuasecurity.com/download_3.html download the newest version,and replace the .so files.

